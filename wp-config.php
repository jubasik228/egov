<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'egov_db' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

define( 'SP_REQUEST_URL', ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] );

define( 'WP_SITEURL', SP_REQUEST_URL );
define( 'WP_HOME', SP_REQUEST_URL );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K(N#NV^]Tzm1Op6~&%R;iJ[/G1&VW^5#+_01fq]F#i%#HmYe;pwj3(B/<cLec` _' );
define( 'SECURE_AUTH_KEY',  '2T-3`Z:Kxp*N;<Z {BR~T~51LEhHd6>Le/~dRt7wgZKD^e63FQdHXQuR-MR3/{{j' );
define( 'LOGGED_IN_KEY',    '})R6my3pWV(0@A#]qDAkQi-xwirDy;8s+nS)E.@}Vd_W,;8ku%n ;YZ(}UYyr+YZ' );
define( 'NONCE_KEY',        'a@wM*x``d~:pe,z$Auk7GG)Vne5ALCS$<BV|#G[oX5&]au3W6FKGFkqj,IS|Kc7$' );
define( 'AUTH_SALT',        'pm-(:iQc$Ftf1y|JJr)fJYD~q.l6DoZ|Qf 3:1r&K~rjjkhQH_ESkj-^<rK^J]$4' );
define( 'SECURE_AUTH_SALT', 'Im0H$Mp|a*Nw[PI{M&nP2Q1ere%CYAh={pHjMe2u;.$[#S;/K dYfYeAs8,+?iK`' );
define( 'LOGGED_IN_SALT',   '1) JLKZ5%_9`DAu2-I6653DThKs A }^6l}x((8hwbZU@Mr)[&.M;TupFY(eRPIm' );
define( 'NONCE_SALT',       'vb$QW Twd2iB/R(v@1U-FXIZbjSWOZzUB?};r{x$-;=}ooAe)oVX$w4x6L4F#YCH' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', 0 );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
