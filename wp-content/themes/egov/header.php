
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="robots" content="none">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    wp_head();
    ?>

    <style>body{-webkit-animation:start .65s;-moz-animation:start .65s;-ms-animation:start .65s;animation:start .65s}@-webkit-keyframes start{from{visibility:hidden;opacity:0}to{visibility:visible;opacity:1}}@-moz-keyframes start{from{visibility:hidden;opacity:0}to{visibility:visible;opacity:1}}@-ms-keyframes start{from{visibility:hidden;opacity:0}to{visibility:visible;opacity:1}}@-o-keyframes start{from{visibility:hidden;opacity:0}to{visibility:visible;opacity:1}}@keyframes start{from{visibility:hidden;opacity:0}to{visibility:visible;opacity:1}}</style>

<body>
<header class="wrap header">
    <nav class="cn">
        <?php wp_nav_menu( [
            'theme_location' => 'main_menu',
            'items_wrap'     => '<ul class="menu"><li class="menu__li-logo"><a href="/" class="logo"><img class="img" src="/src/img/logo.svg" alt="egov"></a></li>%3$s</ul>'
        ] ); ?>
        <ul class="menu" style="display: none !important;;">
            <li class="menu__li-logo"><a href="/" class="logo"><img class="img" src="/src/img/logo.svg" alt="egov"></a></li>

            <li class="menu__li active"><a href="<?php echo home_url(); ?>" class="link">Главная</a></li>
            <li class="menu__li"><a href="regions.html" class="link">Регионы</a></li>
            <li class="menu__li"><a href="about.html " class="link">О нас</a></li>
            <li class="menu__li"><a href="services.html" class="link">Сервисы</a></li>
            <li class="menu__li"><a href="<?php echo home_url(); ?>" class="link">E Gov законы</a></li>

            <li class="menu__li dropdown" style="display: none;">
                <button type="button" class="btn-tr dropdown_btn">Ru<svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.83893 0.288724L6.00537 4.11864L10.1718 0.288724C10.5906 -0.0962415 11.2671 -0.0962415 11.6859 0.288724C12.1047 0.67369 12.1047 1.29556 11.6859 1.68052L6.75705 6.21128C6.33825 6.59624 5.66174 6.59624 5.24295 6.21128L0.314094 1.68052C-0.104697 1.29556 -0.104697 0.67369 0.314095 0.288724C0.732886 -0.086371 1.42013 -0.0962419 1.83893 0.288724Z" fill="#0047FF"/></svg></button>
                <div class="dropdown_menu">
                    <ul>
                        <li><a href="#" class="dropdown__link">en</a></li>
                        <li><a href="#" class="dropdown__link">ua</a></li>
                        <li><a href="#" class="dropdown__link">ru</a></li>
                    </ul>
                </div>
            </li>

            <li><button type="button" class="btn-tr btn-mob open"><svg width="30" height="16" viewBox="0 0 30 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M1 0C0.447708 0 0 0.447716 0 1C0 1.55228 0.447723 2 1 2H29C29.5523 2 30 1.55228 30 1C30 0.447716 29.5523 0 29 0H1ZM0 8C0 7.44772 0.447708 7 1 7H29C29.5523 7 30 7.44772 30 8C30 8.55228 29.5523 9 29 9H1C0.447723 9 0 8.55228 0 8ZM0 15C0 14.4477 0.447708 14 1 14H29C29.5523 14 30 14.4477 30 15C30 15.5523 29.5523 16 29 16H1C0.447723 16 0 15.5523 0 15Z" fill="#1E1E1E"/></svg></button></li>
        </ul>
    </nav>

    <nav class="mob-menu">
        <div class="wrap">
            <ul class="menu">
                <li class="dropdown">
                    <button type="button" class="btn-tr dropdown_btn">Ru<svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.83893 0.288724L6.00537 4.11864L10.1718 0.288724C10.5906 -0.0962415 11.2671 -0.0962415 11.6859 0.288724C12.1047 0.67369 12.1047 1.29556 11.6859 1.68052L6.75705 6.21128C6.33825 6.59624 5.66174 6.59624 5.24295 6.21128L0.314094 1.68052C-0.104697 1.29556 -0.104697 0.67369 0.314095 0.288724C0.732886 -0.086371 1.42013 -0.0962419 1.83893 0.288724Z" fill="#0047FF"/></svg></button>
                    <div class="dropdown_menu" style="display: none;">
                        <ul>
                            <li><a href="#" class="dropdown__link">en</a></li>
                            <li><a href="#" class="dropdown__link">ua</a></li>
                            <li><a href="#" class="dropdown__link">ru</a></li>
                        </ul>
                    </div>
                </li>
                <li><button class="btn-tr btn-mob close"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="0.686523" y="21.8997" width="30" height="2" rx="1" transform="rotate(-45 0.686523 21.8997)" fill="black"/><rect x="2.59082" y="0.686279" width="30" height="2" rx="1" transform="rotate(45 2.59082 0.686279)" fill="black"/></svg></button></li>
            </ul>
        </div>

        <div class="mob-menu_nav">
            <ul>
                <li><a href="regions.html" class="link">регионы</a></li>

                <li><a href="about.html" class="link">о нас</a></li>

                <li><a href="services.html" class="link">сервисы</a></li>

                <li><a href="e-gov_laws.html" class="link">E Gov законы</a></li>

            </ul>
        </div>
    </nav>
</header>
