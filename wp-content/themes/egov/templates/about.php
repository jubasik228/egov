<?php
/**
 * Template Name: About
 */
get_header();
?>
    <section class="wrap head not-home">
        <div class="cn__img">
            <img class="img" src="/src/img/head.jpg" alt="">
        </div>
        <div class="cn">
            <h1>О нас</h1>
            <p class="title-des">Мы занимаемся развитием и популяризацией E-gov решений в Украине. Наш сайт является сервисом - агрегатором всех e-gov решений в стране. Наша команда постоянно следит за тем, чтоб информация была актуальна и постоянно обновлялась.</p>
        </div>
    </section>

    <section class="wrap how-we-work">
        <div class="cn">
            <h2 class="title">Наши основные направления</h2>
            <ul class="gall">
                <li class="gall__it">
                    <strong>Всеукраинская база<br>e-service</strong>
                    <p>Для удобства мы собрали все e-service в одном месте. Теперь вам не нужно искать в интернете нужную услугу. Все можно найти все на одной платформе</p>
                </li>
                <li class="gall__it">
                    <strong>Гранты на развитие проектов в сфере e-gov</strong>
                    <p>Отбираем заявки на грантовые проекты в сфере e-gov и обрабатываем их в сотрудничестве с венчурными организациями</p>
                </li>
                <li class="gall__it">
                    <strong>Стать волонтером в своем регионе</strong>
                    <p>Хочешь стать амбасадором своей области? Присоединяйся</p>
                </li>
                <li class="gall__it">
                    <strong>Удобная база региональных<br> e-service</strong>
                    <p>Платформа разделена на всеукраинские и региональные сайты. Тут вы сможете с легкостью найти все электронные сервисы в вашем регионе</p>
                </li>
                <li class="gall__it">
                    <strong>Сотрудничество между государственными органами</strong>
                    <p>Работам над сотрудничеством между горсударственными органами для передачи знаний, наработок и популяризации электронных решений</p>
                </li>
                <li class="gall__it">
                    <strong>Сотрудничество и иностранными партнерами</strong>
                    <p>Сотрудничаем с иностранными партнерами для передачи опыта и развития электронных регений в Украине</p>
                </li>
            </ul>
        </div>
    </section>

    <section class="wrap how-we-work-card">
        <div class="cn">
            <div class="content-video">
                <h2 class="title mobile">Что такое e-service</h2>
                <video poster="/src/img/poster.jpg" id="player" playsinline controls>
                    <source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" type="video/mp4"/>
                </video>
            </div>
            <div class="content">
                <h2 class="title desktop">Что такое e-service</h2>
                <p>После вам будут доступны сервисы Портал E Gov это решение для просмотра электронных сервисов в Украине и для единого доступа к ним. Для вашего удобства сервисы разбиты на всеукраинеские и региональные категории</p>
            </div>
        </div>
    </section>

    <section class="wrap contacts">
        <div class="cn">
            <h2 class="title">Будем на связи</h2>
            <ul class="contacts__gall">
                <li>
                    <figure>
                        <p class="cn__img"><img src="/src/img/contacts/igor.jpg" alt="Игорь Николаев" class="img"></p>
                        <figcaption><strong class="contacts__name">Игорь Николаев</strong></figcaption>
                    </figure>
                    <p class="contacts__pos">Сотрудничество и партнерство</p>
                    <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                    <div>
                        <a href="#" class="contacts__link in"></a>
                        <a href="#" class="contacts__link twi"></a>
                        <a href="#" class="contacts__link fac"></a>
                    </div>
                </li>
                <li>
                    <figure>
                        <p class="cn__img"><img src="/src/img/contacts/viki.jpg" alt="Виктория Петрухина" class="img"></p>
                        <figcaption><strong class="contacts__name">Виктория Петрухина</strong></figcaption>
                    </figure>
                    <p class="contacts__pos">PR и работа с прессой</p>
                    <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                    <div>
                        <a href="#" class="contacts__link in"></a>
                        <a href="#" class="contacts__link twi"></a>
                        <a href="#" class="contacts__link fac"></a>
                    </div>
                </li>
                <li>
                    <figure>
                        <p class="cn__img"><img src="/src/img/contacts/alex.jpg" alt="Александр Алименко" class="img"></p>
                        <figcaption><strong class="contacts__name">Александр Алименко</strong></figcaption>
                    </figure>
                    <p class="contacts__pos">Технические решения</p>
                    <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                    <div>
                        <a href="#" class="contacts__link in"></a>
                        <a href="#" class="contacts__link twi"></a>
                        <a href="#" class="contacts__link fac"></a>
                    </div>
                </li>
                <li>
                    <figure>
                        <p class="cn__img"><img src="/src/img/contacts/viki.jpg" alt="Ольга Симсон" class="img"></p>
                        <figcaption><strong class="contacts__name">Ольга Симсон</strong></figcaption>
                    </figure>
                    <p class="contacts__pos">Юридическое сопровождение</p>
                    <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                    <div>
                        <a href="#" class="contacts__link in"></a>
                        <a href="#" class="contacts__link twi"></a>
                        <a href="#" class="contacts__link fac"></a>
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <div class="map">
        <div id="map" data-lat="50.424144" data-lng="30.512645"></div>
        <div class="wrap map__contacts">
            <div class="map__address">
                <p class="map__title">
                    <span><svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.75244 0C2.57216 0 0 2.57216 0 5.75244C0 9.17925 3.63225 13.9045 5.12789 15.7042C5.4566 16.0986 6.0565 16.0986 6.38521 15.7042C7.87262 13.9045 11.5049 9.17925 11.5049 5.75244C11.5049 2.57216 8.93272 0 5.75244 0ZM5.75244 7.80688C4.61839 7.80688 3.698 6.88649 3.698 5.75244C3.698 4.61839 4.61839 3.698 5.75244 3.698C6.88649 3.698 7.80688 4.61839 7.80688 5.75244C7.80688 6.88649 6.88649 7.80688 5.75244 7.80688Z" fill="white"/></svg></span>
                    Адреса
                </p>
                <a href="#" class="map__link" data-lat="50.424144" data-lng="30.512645">"01133 Украина, Киев, ул. Ямская 35, 17 этаж"</a>
            </div>
            <div>
                <p class="map__title">
                    <span><svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.99921 7.99917L6.01978 6.2666L0.360352 11.118C0.566066 11.3089 0.84378 11.4277 1.15007 11.4277H14.8483C15.1535 11.4277 15.4301 11.3089 15.6346 11.118L9.97863 6.2666L7.99921 7.99917Z" fill="white"/><path d="M15.6387 0.309714C15.433 0.117714 15.1564 0 14.849 0H1.15071C0.845567 0 0.568995 0.118857 0.363281 0.312L7.99985 6.85714L15.6387 0.309714Z" fill="white"/><path d="M10.4766 5.80457L16 10.4983V1L10.4766 5.80457Z" fill="white"/><path d="M0 1.00293V10.4978L5.52343 5.80407L0 1.00293Z" fill="white"/></svg></span>
                    Почта
                </p>
                <a href="mailto:info@eserv.gov.ua" class="map__link">info@eserv.gov.ua</a>
            </div>
        </div>
    </div>

    <section class="wrap agent">
        <div class="cn">
            <h2 class="title">Наши представители в Регионах</h2>
            <div class="agent__cn">
                <ul class="agent__gall">
                    <li class="active">
                        <header class="agent__head accordion__header">
                            <p class="agent__location">АР Крым</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Днепропетровская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Донецкая</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Ивано-Франковская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Львовская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Ровенская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Херсонская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Черкасская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Винницкая</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Житомирская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Кировоградская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Сумская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Николаевская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Хмельницкая</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Волынская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Закарпатская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Киевская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Одесская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Тернопольская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Черниговская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Днепропетровская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Запорожская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Луганская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Черновицкая</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Харьковская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <header class="agent__head accordion__header">
                            <p class="agent__location">Полтавская</p>
                            <hr>
                            <p class="agent__name">Николаев Игорь</p>
                        </header>
                        <div class="agent__content accordion__content">
                            <a href="mailto:info@eserv.gov.ua" class="contacts__mail">info@eserv.gov.ua</a>
                            <p>
                                <a href="#" class="contacts__link in"></a>
                                <a href="#" class="contacts__link twi"></a>
                                <a href="#" class="contacts__link fac"></a>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </section>

    <section class="wrap material">
        <div class="cn">
            <h2 class="title">Информационные материалы</h2>
            <ul class="material__gall">
                <li>
                    <figure>
                        <p class="cn__img"><img src="xxxHTMLLINKxxx0.20613417256649980.49754924528590583xxx" alt="" class="img"></p>
                        <figcaption>Onepager (e-serv) in Ukraine</figcaption>
                    </figure>
                    <a href="#" class="btn blue" download="">Скачати</a>
                </li>
                <li>
                    <figure>
                        <p class="cn__img"><img src="xxxHTMLLINKxxx0.69163594800225960.11216178728521564xxx" alt="" class="img"></p>
                        <figcaption>Презентация сервиса</figcaption>
                    </figure>
                    <a href="#" class="btn blue" download="">Скачати</a>
                </li>
                <li>
                    <figure>
                        <p class="cn__img"><img src="xxxHTMLLINKxxx0.63687953799095730.6728252025933885xxx" alt="" class="img"></p>
                        <figcaption>Презентация сервиса</figcaption>
                    </figure>
                    <a href="#" class="btn blue" download="">Скачати</a>
                </li>
            </ul>
        </div>
    </section>

    <section class="wrap subscribe">
        <div class="cn">
            <h2 class="title">Подпишитесь на рассылку</h2>
            <p class="title-des"> Узнавай о новых сервисах и законадательстве в Украине и других станах</p>
            <form action="#" method="post">
                <input type="email" name="email" placeholder="Введите ваш E-mail" required>
                <button type="submit" class="btn black">Подписаться</button>
                <button type="submit" class="btn blue">Telegram <span class="ico"><svg width="28" height="26" viewBox="0 0 28 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M21.255 24.8841C22.9325 25.618 23.5616 24.0803 23.5616 24.0803L28 1.78335C27.9651 0.280584 25.9381 1.18924 25.9381 1.18924L1.08994 10.9398C1.08994 10.9398 -0.0982929 11.3591 0.00655147 12.0931C0.111396 12.827 1.055 13.1764 1.055 13.1764L7.31071 15.2733C7.31071 15.2733 9.19791 21.4591 9.58234 22.6474C9.93182 23.8007 10.2464 23.8356 10.2464 23.8356C10.5958 23.9754 10.9104 23.7308 10.9104 23.7308L14.9643 20.0612L21.255 24.8841ZM22.3384 5.73245C22.3384 5.73245 23.2121 5.20823 23.1772 5.73245C23.1772 5.73245 23.317 5.80235 22.8626 6.29162C22.4433 6.711 12.5529 15.5878 11.2249 16.7761C11.1201 16.846 11.0502 16.9508 11.0502 17.0906L10.6657 20.3757C10.5958 20.7252 10.1415 20.7601 10.0367 20.4456L8.39411 15.0636C8.32421 14.8539 8.39411 14.6093 8.6038 14.4695L22.3384 5.73245Z" fill="white"/></svg></span></button>
            </form>
        </div>
    </section>
<?php
get_footer();
