<?php
get_header();

$posts_array = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'services',
        'tax_query' => array(
            array(
                'taxonomy' => 'regions',
                'field' => 'term_id',
                'terms' => get_queried_object()->term_id,
            )
        )
    )
);

$all_services_type = array();
foreach ($posts_array as $item){
    if (empty($item->post_parent)) {
        $item_term = get_the_terms($item->ID, 'services_type');
        if (!in_array($item_term[0]->name, $all_services_type)) {
            $all_services_type[$item_term[0]->slug]['name'] = $item_term[0]->name;
        }
        $all_services_type[$item_term[0]->slug]['services'][] = $item;
    }
}

?>
    <div class="wrap breadcrumbs">
        <nav class="cn">

            <ul>
                <li class="breadcrumbs__li"><a href="<?php echo home_url(); ?>" class="breadcrumbs__link">Главная</a></li>
                <li class="breadcrumbs__arrow"><svg width="12" height="9" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M9.80453 0L7.87598 1.6429L11.0964 4.80195H0V7.23949H11.0964L7.87598 10.34L9.80453 12L16 5.96953L9.80453 0Z" fill="#3355FF"/></svg></li>
                <li class="breadcrumbs__li"><a href="<?php echo get_term_link(get_queried_object(), 'regions')?>" class="breadcrumbs__link active">E GOV ЗАКОНЫ</a></li>
            </ul>
        </nav>
    </div>

    <section class="wrap service service-laws">
        <div class="cn">
            <div class="info">
                <h1 class="title big"><?php echo single_term_title() ?></h1>
                <figure class="cn__img">
                    <img class="img" src="/src/img/select-service.svg" alt="">
                </figure>
                <p>Многие активности в Украине и любой другой правовой стране невозможны без участия государства так как именно государство принимает законы, которые разрешают электронный документооборот вместо бумажного, оплату картой вместе оплаты наличными и так далее</p>
                <p>Поэтому мы представили список всех законопроектов, которые есть в Украине на данный момент. Для вашего удобства они разбиты на категории и даты принятия</p>
            </div>
            <section class="get-select-service">
                <?php if(count($all_services_type)): ?>
                <ul class="get-select-service__ctg accordion mobile">
                    <?php foreach ($all_services_type as $key=>$item): ?>
                    <li>
                        <header class="select-link select-accordion__header" data-hash="#<?php echo $key; ?>"><?php echo $item['name']; ?><span class="count"><?php echo count($item['services']); ?></span></header>
                        <ul class="select-accordion__content">
                            <?php foreach ($item['services'] as $service): ?>
                    <li>
                        <?php
                        $serv_link = get_field('external_url',$service) ? get_field('external_url',$service) : get_permalink($service);

                        ?>
                        <a href="<?php echo $serv_link; ?>" class="accordion__it__link">
                            <p>
                                <strong><?php echo get_the_title($service);?></strong>
                            </p>
                            <p><?php echo get_field('laws_with',$service) ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>


                        </ul>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
                <div class="get-select-service__ctg desktop">
                    <?php if(count($all_services_type)): ?>
                    <div class="cn__select-link">
                        <?php foreach ($all_services_type as $key=>$item): ?>
                        <a href="#<?php echo $key; ?>" class="select-link"><?php echo $item['name']; ?><span class="count"><?php echo count($item['services']); ?></span></a>
                        <?php endforeach; ?>
                    </div>
                    <?php foreach ($all_services_type as $key=>$item): ?>
                    <div class="accordion accordion-tabs" data-hash="#<?php echo $key; ?>">
                        <ul class="accordion-tabs__it">
                            <?php foreach ($item['services'] as $service): ?>
                                <li>
                                    <?php
                                    $serv_link = get_field('external_url',$service) ? get_field('external_url',$service) : get_permalink($service);

                                    ?>
                                    <a href="<?php echo $serv_link; ?>" class="accordion__it__link">
                                        <p>
                                            <strong><?php echo get_the_title($service);?></strong>
                                        </p>
                                        <p><?php echo get_field('laws_with',$service) ?></p>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </section>


    <section class="wrap to_partner">
        <div class="cn">
            <h2 class="title">Хочешь развивать E GOV в Украине или стать партнером?</h2>
            <div class="info">
                <div class="des">
                    <h3>Стань амбасадором в своем городе!</h3>
                    <p>Мы занимаемся популяризацией e-gov в Украине и упрощаем использование сервисов для всех людей. Если вы заметили на сайте неточности, или считаете, что мы не добавили какой-то e-сервис — напишите нам</p>
                </div>
                <div class="cn__btn">
                    <button class="btn blue reqMessage">
                        <span class="ico-left"><svg width="24" height="19" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21.6 0H2.4C1.08 0 0.012 1.06875 0.012 2.375L0 16.625C0 17.9312 1.08 19 2.4 19H21.6C22.92 19 24 17.9312 24 16.625V2.375C24 1.06875 22.92 0 21.6 0ZM21.6 16.625H2.4V4.75L10.092 9.50742C11.2594 10.2294 12.7406 10.2294 13.908 9.50742L21.6 4.75V16.625ZM13.908 7.13242C12.7406 7.85443 11.2594 7.85443 10.092 7.13242L2.4 2.375H21.6L13.908 7.13242Z" fill="white"/></svg></span>
                        Отправить запрос
                    </button>
                    <a href="services.html" class="btn brd">
                        <span class="ico-left"><svg width="12" height="19" viewBox="0 0 12 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.92404 0C3.05496 0 0.704291 1.82041 0.0377344 4.36047C-0.172708 5.16241 0.528162 5.84486 1.36708 5.84486C2.20599 5.84486 2.86551 5.13064 3.29411 4.41849C3.83062 3.52704 4.83379 2.89716 5.92404 2.89716C7.53851 2.89716 8.96202 4.27837 8.96202 5.84486C8.96202 8.44948 5.39729 8.5159 4.56751 11.8428C4.36575 12.6518 5.08034 13.3404 5.92404 13.3404C6.76775 13.3404 7.41729 12.6285 7.79789 11.885C8.85988 9.81021 12 9.00788 12 5.84486C12 2.54344 9.32658 0 5.92404 0ZM4.18806 17.3156C4.18806 16.3853 4.95173 15.6312 5.89378 15.6312H5.95431C6.89635 15.6312 7.66003 16.3853 7.66003 17.3156C7.66003 18.2459 6.89635 19 5.95431 19H5.89378C4.95174 19 4.18806 18.2459 4.18806 17.3156Z" fill="#1E1E1E"/></svg></span>
                        Узнать о наших сервисах
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
