<?php
get_header();
$posts_array = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'services',
        'tax_query' => array(
            array(
                'taxonomy' => 'regions',
                'field' => 'term_id',
                'terms' => 2,
            )
        )
    )
);
$services_count = 0;
$all_services_type = array();
foreach ($posts_array as $item){
    if (empty($item->post_parent)) {
        $item_term = get_the_terms($item->ID, 'services_type');
        if (!in_array($item_term[0]->name, $all_services_type)) {
            $all_services_type[$item_term[0]->slug]['name'] = $item_term[0]->name;
        }
        $all_services_type[$item_term[0]->slug]['services'][] = $item;
        $services_count++;
    }
}
?>
    <section class="wrap head">
        <div class="cn__img">
            <img class="img" src="/src/img/head.jpg" alt="">
        </div>
        <div class="cn">
            <h1>E-GOV – все сервисы Украины</h1>
            <p class="title-des">Портал <b>E-GOV</b> это агрегатор всех всеукраинских и региональных электронных сервисов в стране</p>
        </div>
    </section>

    <section class="wrap get-select-service">
        <div class="cn">
            <div class="get-select-service__info">
                <h2 class="title">Выберете услугу которую вы хотите получить</h2>
                <div class="info-count">
                    <p><b><?php echo $services_count;?></b> украинских сервисов</p>
                    <p><b><?php echo count($all_services_type);?></b> категорий</p>
                </div>
            </div>
            <form action="" method="post" class="search">
                <input class="search__input" type="text" name="q" placeholder="Впишите в этом поле услугу, которую вы хотите найти">
                <button type="submit" class="btn-tr"><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.7929 11.2937C12.6053 11.1057 12.3506 11 12.085 11H11.7099L11.4299 10.73C12.6299 9.33001 13.2499 7.42002 12.9099 5.39002C12.4399 2.61002 10.1199 0.390015 7.3199 0.0500152C3.0899 -0.469985 -0.460103 3.09001 0.0498967 7.32001C0.389897 10.12 2.6099 12.44 5.3899 12.91C7.4199 13.25 9.3299 12.63 10.7299 11.43L10.9999 11.71V12.0851C10.9999 12.3507 11.1056 12.6054 11.2936 12.7931L15.2599 16.75C15.6699 17.16 16.3299 17.16 16.7399 16.75L16.7499 16.74C17.1599 16.33 17.1599 15.67 16.7499 15.26L12.7929 11.2937ZM6.4999 11C4.0099 11 1.9999 8.99002 1.9999 6.50002C1.9999 4.01002 4.0099 2.00002 6.4999 2.00002C8.9899 2.00002 10.9999 4.01002 10.9999 6.50002C10.9999 8.99002 8.9899 11 6.4999 11Z" fill="black"/></svg></button>
                <ul class="search__dropdown">
                    <li>
                        <a href="service.html">ДАБІ України Електронна система здійснення декларативних процедур у будівництві</a>
                    </li>
                    <li>
                        <a href="service_car-numbers.html">ДАБІ України Електронна система здійснення декларативних процедур у будівництві</a>
                    </li>
                    <li>
                        <a href="service.html">ДАБІ України Електронна система здійснення декларативних процедур у будівництві</a>
                    </li>
                    <li>
                        <a href="service.html">ДАБІ України Електронна система здійснення декларативних процедур у будівництві</a>
                    </li>
                    <li>
                        <a href="service.html">ДАБІ України Електронна система здійснення декларативних процедур у будівництві</a>
                    </li>
                </ul>
            </form>



            <?php if(count($all_services_type)): ?>
                <ul class="get-select-service__ctg accordion mobile">
                    <?php foreach ($all_services_type as $key=>$item): ?>
                        <li>
                            <header class="select-link select-accordion__header" data-hash="#<?php echo $key; ?>"><?php echo $item['name']; ?><span class="count"><?php echo count($item['services']); ?></span></header>
                            <div class="select-accordion__content">
                                <?php foreach ($item['services'] as $service): ?>
                                    <?php
                                    $childrens = get_children( [
                                        'post_parent' => $service->ID,
                                        'post_type'   => 'services',
                                        'numberposts' => -1,
                                        'post_status' => 'publish'
                                    ] );
                                    ?>
                                    <div class="accordion__it">
                                        <?php if( $childrens ): ?>
                                            <header class="accordion__header">
                                                <div class="accordion__info">
                                                    <strong class="accordion__title"><?php echo get_the_title($service);?></strong>

                                                    <div class="accordion__ctg-count">
                                                        <p>Раздел: <b><?php echo $item['name']; ?></b></p>
                                                        <p>Подкатегорий: <b><?php echo count($childrens); ?></b></p>
                                                    </div>
                                                </div>
                                                <span class="ico"><svg width="12" height="6" viewBox="0 0 12 6" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.86049 0.266514L5.67973 3.80182L9.49897 0.266515C9.88286 -0.0888383 10.503 -0.0888383 10.8869 0.266515C11.2708 0.621868 11.2708 1.1959 10.8869 1.55125L6.36876 5.73349C5.98487 6.08884 5.36474 6.08884 4.98084 5.73348L0.462725 1.55125C0.0788316 1.1959 0.0788316 0.621867 0.462725 0.266514C0.846617 -0.0797271 1.47659 -0.0888387 1.86049 0.266514Z" fill="#1E1E1E"/></svg></span>
                                            </header>
                                            <div class="accordion__content">
                                                <ul>
                                                    <?php foreach( $childrens as $children ): ?>
                                                        <?php
                                                        $serv_link = get_field('external_url',$children->ID) ? get_field('external_url',$children->ID) : get_permalink($children->ID);
                                                        ?>
                                                        <li><a href="<?php echo $serv_link; ?>"><?php echo get_the_title($children);?></a></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        <?php else: ?>
                                            <header class="accordion__header">
                                                <a class="accordion__info" href="<?php echo get_permalink($service); ?>">
                                                    <strong class="accordion__title"><?php echo get_the_title($service);?></strong>

                                                    <div class="accordion__ctg-count">
                                                        <p>Раздел: <b><?php echo $item['name']; ?></b></p>
                                                    </div>
                                                </a>
                                            </header>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>


                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <div class="get-select-service__ctg desktop">
                <?php if(count($all_services_type)): ?>
                    <div class="cn__select-link">
                        <?php foreach ($all_services_type as $key=>$item): ?>
                            <a href="#<?php echo $key; ?>" class="select-link"><?php echo $item['name']; ?><span class="count"><?php echo count($item['services']); ?></span></a>
                        <?php endforeach; ?>
                    </div>
                    <?php foreach ($all_services_type as $key=>$item): ?>
                        <div class="accordion accordion-tabs" data-hash="#<?php echo $key; ?>">
                            <ul class="accordion-tabs__it">
                                <?php foreach ($item['services'] as $service): ?>
                                    <?php
                                    $childrens = get_children( [
                                        'post_parent' => $service->ID,
                                        'post_type'   => 'services',
                                        'numberposts' => -1,
                                        'post_status' => 'publish'
                                    ] );
                                    ?>
                                    <li class="accordion__it">
                                        <?php if( $childrens ): ?>
                                            <header class="accordion__header">
                                                <div class="accordion__info">
                                                    <strong class="accordion__title"><?php echo get_the_title($service);?></strong>

                                                    <div class="accordion__ctg-count">
                                                        <p>Раздел: <b><?php echo $item['name']; ?></b></p>
                                                        <p>Подкатегорий: <b><?php echo count($childrens); ?></b></p>
                                                    </div>
                                                </div>
                                                <span class="ico"><svg width="12" height="6" viewBox="0 0 12 6" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.86049 0.266514L5.67973 3.80182L9.49897 0.266515C9.88286 -0.0888383 10.503 -0.0888383 10.8869 0.266515C11.2708 0.621868 11.2708 1.1959 10.8869 1.55125L6.36876 5.73349C5.98487 6.08884 5.36474 6.08884 4.98084 5.73348L0.462725 1.55125C0.0788316 1.1959 0.0788316 0.621867 0.462725 0.266514C0.846617 -0.0797271 1.47659 -0.0888387 1.86049 0.266514Z" fill="#1E1E1E"/></svg></span>
                                            </header>
                                            <div class="accordion__content">
                                                <ul>
                                                    <?php foreach( $childrens as $children ): ?>
                                                        <?php
                                                        $serv_link = get_field('external_url',$children->ID) ? get_field('external_url',$children->ID) : get_permalink($children->ID);
                                                        ?>
                                                        <li><a href="<?php echo $serv_link; ?>"><?php echo get_the_title($children);?></a></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        <?php else: ?>
                                            <header class="accordion__header">
                                                <a class="accordion__info" href="<?php echo get_permalink($service); ?>">
                                                    <strong class="accordion__title"><?php echo get_the_title($service);?></strong>

                                                    <div class="accordion__ctg-count">
                                                        <p>Раздел: <b><?php echo $item['name']; ?></b></p>
                                                    </div>
                                                </a>
                                            </header>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="wrap select-service">
        <div class="cn">
            <div class="info">
                <h2 class="title">Выберите услугу в вашем регионе</h2>
                <p class="title-des">Если вам нужно найти электронную услугу в вашем регионе перейдите по ссылке и выберите вашу область</p>
            </div>

            <div class="">
                <div class="cn__img">
                    <img class="img" src="/src/img/select-service.svg" alt="">
                </div>
                <a href="regions.html" class="btn blue">ВЫбЕРИТЕ ВАШ РЕГИОН <span class="ico-right"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.00002 16.463L12.88 12.583L9.00002 8.70299C8.61002 8.31299 8.61002 7.68299 9.00002 7.29299C9.39002 6.90299 10.02 6.90299 10.41 7.29299L15 11.883C15.39 12.273 15.39 12.903 15 13.293L10.41 17.883C10.02 18.273 9.39002 18.273 9.00002 17.883C8.62002 17.493 8.61002 16.853 9.00002 16.463Z" fill="white"/></svg></span></a>
            </div>
        </div>
    </section>

    <section class="wrap blog">
        <div class="cn">
            <h2 class="title">Блог</h2>
            <ul class="gall">
                <li><a href="#" class="gall__it">
                        <figure>
                            <p class="cn__img"><img src="/src/img/blog/blog_1.jpg" alt="Более 200 всеукраинских и региональных сервисов в одном месте" class="img"></p>
                            <figcaption>Более 200 всеукраинских и региональных сервисов в одном месте</figcaption>
                        </figure>
                        <time datetime="2019-12-12">12 декабря 2019</time>
                    </a></li>
                <li><a href="#" class="gall__it">
                        <figure>
                            <p class="cn__img"><img src="/src/img/blog/blog_2.jpg" alt="Более 200 всеукраинских и региональных сервисов в одном месте" class="img"></p>
                            <figcaption>Более 200 всеукраинских и региональных сервисов в одном месте</figcaption>
                        </figure>
                        <time datetime="2019-12-12">12 декабря 2019</time>
                    </a></li>
                <li><a href="#" class="gall__it">
                        <figure>
                            <p class="cn__img"><img src="/src/img/blog/blog_3.jpg" alt="Более 200 всеукраинских и региональных сервисов в одном месте" class="img"></p>
                            <figcaption>Более 200 всеукраинских и региональных сервисов в одном месте</figcaption>
                        </figure>
                        <time datetime="2019-12-12">12 декабря 2019</time>
                    </a></li>
            </ul>
            <a href="#" class="btn black">Смотреть все</a>
        </div>
    </section>

    <section class="wrap e-government">
        <div class="cn">
            <h2 class="title">E-Government в Украине – это</h2>
            <ul class="gall">
                <li class="gall__it">
                    <figure>
                        <p class="cn__img"><img src="/src/img/ico_e_government/Vector.svg" alt="" class="img"></p>
                        <figcaption>200+ всеукраинских сервисов в одном месте</figcaption>
                    </figure>
                </li>
                <li class="gall__it">
                    <figure>
                        <p class="cn__img"><img src="/src/img/ico_e_government/development.svg" alt="" class="img"></p>
                        <figcaption>Постоянное развитие и усовершенствование всех платформ</figcaption>
                    </figure>
                </li>
                <li class="gall__it">
                    <figure>
                        <p class="cn__img"><img src="/src/img/ico_e_government/certificate.svg" alt="" class="img"></p>
                        <figcaption>Наличие более 200 правовых документов для развития сервисов электронного управления</figcaption>
                    </figure>
                </li>
                <li class="gall__it">
                    <figure>
                        <p class="cn__img"><img src="/src/img/ico_e_government/antenna.svg" alt="" class="img"></p>
                        <figcaption>Более 90% покрыто интернетом</figcaption>
                    </figure>
                </li>
            </ul>
        </div>
    </section>

    <section class="wrap partners">
        <div class="cn">
            <h2 class="title">Наши партнеры и сторонники</h2>
            <ul class="gall">
                <li class="gall__it"><img class="img" src="/src/img/partners/img.svg" alt=""></li>
                <li class="gall__it"><img class="img" src="/src/img/partners/img.svg" alt=""></li>
                <li class="gall__it"><img class="img" src="/src/img/partners/img.svg" alt=""></li>
                <li class="gall__it"><img class="img" src="/src/img/partners/img.svg" alt=""></li>
                <li class="gall__it"><img class="img" src="/src/img/partners/img.svg" alt=""></li>
                <li class="gall__it"><img class="img" src="/src/img/partners/img.svg" alt=""></li>
            </ul>
        </div>
    </section>

    <section class="wrap to_partner">
        <div class="cn">
            <h2 class="title">Хочешь развивать E GOV в Украине или стать партнером?</h2>
            <div class="info">
                <div class="des">
                    <h3>Стань амбасадором в своем городе!</h3>
                    <p>Мы занимаемся популяризацией e-gov в Украине и упрощаем использование сервисов для всех людей. Если вы заметили на сайте неточности, или считаете, что мы не добавили какой-то e-сервис — напишите нам</p>
                </div>
                <div class="cn__btn">
                    <button class="btn blue reqMessage">
                        <span class="ico-left"><svg width="24" height="19" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21.6 0H2.4C1.08 0 0.012 1.06875 0.012 2.375L0 16.625C0 17.9312 1.08 19 2.4 19H21.6C22.92 19 24 17.9312 24 16.625V2.375C24 1.06875 22.92 0 21.6 0ZM21.6 16.625H2.4V4.75L10.092 9.50742C11.2594 10.2294 12.7406 10.2294 13.908 9.50742L21.6 4.75V16.625ZM13.908 7.13242C12.7406 7.85443 11.2594 7.85443 10.092 7.13242L2.4 2.375H21.6L13.908 7.13242Z" fill="white"/></svg></span>
                        Отправить запрос
                    </button>
                    <a href="services.html" class="btn brd">
                        <span class="ico-left"><svg width="12" height="19" viewBox="0 0 12 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.92404 0C3.05496 0 0.704291 1.82041 0.0377344 4.36047C-0.172708 5.16241 0.528162 5.84486 1.36708 5.84486C2.20599 5.84486 2.86551 5.13064 3.29411 4.41849C3.83062 3.52704 4.83379 2.89716 5.92404 2.89716C7.53851 2.89716 8.96202 4.27837 8.96202 5.84486C8.96202 8.44948 5.39729 8.5159 4.56751 11.8428C4.36575 12.6518 5.08034 13.3404 5.92404 13.3404C6.76775 13.3404 7.41729 12.6285 7.79789 11.885C8.85988 9.81021 12 9.00788 12 5.84486C12 2.54344 9.32658 0 5.92404 0ZM4.18806 17.3156C4.18806 16.3853 4.95173 15.6312 5.89378 15.6312H5.95431C6.89635 15.6312 7.66003 16.3853 7.66003 17.3156C7.66003 18.2459 6.89635 19 5.95431 19H5.89378C4.95174 19 4.18806 18.2459 4.18806 17.3156Z" fill="#1E1E1E"/></svg></span>
                        Узнать о наших сервисах
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="wrap subscribe">
        <div class="cn">
            <h2 class="title">Подпишитесь на рассылку</h2>
            <p class="title-des"> Узнавай о новых сервисах и законадательстве в Украине и других станах</p>
            <form action="#" method="post">
                <input type="email" name="email" placeholder="Введите ваш E-mail" required>
                <button type="submit" class="btn black">Подписаться</button>
                <button type="submit" class="btn blue">Telegram <span class="ico"><svg width="28" height="26" viewBox="0 0 28 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M21.255 24.8841C22.9325 25.618 23.5616 24.0803 23.5616 24.0803L28 1.78335C27.9651 0.280584 25.9381 1.18924 25.9381 1.18924L1.08994 10.9398C1.08994 10.9398 -0.0982929 11.3591 0.00655147 12.0931C0.111396 12.827 1.055 13.1764 1.055 13.1764L7.31071 15.2733C7.31071 15.2733 9.19791 21.4591 9.58234 22.6474C9.93182 23.8007 10.2464 23.8356 10.2464 23.8356C10.5958 23.9754 10.9104 23.7308 10.9104 23.7308L14.9643 20.0612L21.255 24.8841ZM22.3384 5.73245C22.3384 5.73245 23.2121 5.20823 23.1772 5.73245C23.1772 5.73245 23.317 5.80235 22.8626 6.29162C22.4433 6.711 12.5529 15.5878 11.2249 16.7761C11.1201 16.846 11.0502 16.9508 11.0502 17.0906L10.6657 20.3757C10.5958 20.7252 10.1415 20.7601 10.0367 20.4456L8.39411 15.0636C8.32421 14.8539 8.39411 14.6093 8.6038 14.4695L22.3384 5.73245Z" fill="white"/></svg></span></button>
            </form>
        </div>
    </section>
<?php
get_footer();
