<footer class="footer wrap">
    <div class="cn">
        <div class="content">
            <div class="info">
                <a href="<?php echo home_url(); ?>" class="logo"><img src="/src/img/logo.svg" alt="EGov" class="img"></a>
                <p class="des">Если у вас есть любые предложения или замечания — напишите нам</p>
                <a class="mail" href="mailto:info@egov.in.ua">info@egov.in.ua</a>
                <a class="address" href="#">улица Мечникова 22 Киев, Украина</a>
            </div>
            <nav class="nav">
                <div class="nav__it">
                    <p class="title">страницы</p>
                    <ul>
                        <li><a class="link" href="./">Главная</a></li>
                        <li><a class="link" href="regions.html">Регионы</a></li>
                        <li><a class="link" href="about.html">О нас</a></li>
                        <li><a class="link" href="services.html">Сервисы</a></li>
                        <li><a class="link" href="e-gov_laws.html">E Gov законы</a></li>
                    </ul>
                </div>

                <div class="nav__it">
                    <p class="title">информация</p>
                    <ul>
                        <li><a class="link" href="./">Главная</a></li>
                        <li><a class="link" href="regions.html">Регионы</a></li>
                        <li><a class="link" href="about.html">О нас</a></li>
                        <li><a class="link" href="services.html">Сервисы</a></li>
                        <li><a class="link" href="e-gov_laws.html">E Gov законы</a></li>
                    </ul>
                </div>

                <div class="nav__it">
                    <p class="title">Следите на нами</p>
                    <ul>
                        <li><a class="link" href="#" target="_blank">Telegram</a></li>
                        <li><a class="link" href="#" target="_blank">Instagram</a></li>
                        <li><a class="link" href="#" target="_blank">Facebook</a></li>
                        <li><a class="link" href="#" target="_blank">Twitter</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="copy">© Copyright 2019</div>
    </div>
</footer>

<div class="modal post-message">
    <div class="cn">
        <button class="btn-tr modal-close"><svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="32.5731" height="3.25731" rx="1.62866" transform="matrix(0.706822 0.707391 -0.706822 0.707391 2.91797 0.0770264)" fill="#333333"/><rect width="32.5731" height="3.25731" rx="1.62866" transform="matrix(-0.706822 0.707391 -0.706822 -0.707391 25.9414 2.38123)" fill="#333333"/></svg></button>

        <form action="#" method="post" class="wrap">
            <strong>Наш менеджер поможет Вам</strong>
            <label class="req">
                <span>Ваш телефон</span>
                <input type="tel" name="phone" placeholder="095 00 00 000" required>
            </label>
            <label class="req">
                <span>Ваш E-mail</span>
                <input type="email" name="email" placeholder="example@email.com" required>
            </label>
            <label class="req">
                <span>Ваше Сообщение</span>
                <textarea type="text" name="message"></textarea>
            </label>
            <button type="submit" class="btn blue">Отправить</button>
        </form>
    </div>
</div>
<div class="modal sent">
    <div class="cn">
        <div class="ico"><svg width="44" height="31" viewBox="0 0 44 31" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M42.7992 1L17.1992 29.8L1.19922 17" stroke="white" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/></svg></div>
        <strong>Спасибо! Сообщение отправлено</strong>
        <p>Мы свяжемся с Вами в течение часа. Обращения обрабатываются с 10 до 18 с понедельника по пятницу</p>
        <a href="./" class="btn blue">Перейти на главную</a>
    </div>
</div>
<div class="modal sent error">
    <div class="cn">
        <div class="ico"><svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="32.5731" height="3.25731" rx="1.62866" transform="matrix(0.706822 0.707391 -0.706822 0.707391 2.91797 0.0770264)" fill="#FFFFFF"/><rect width="32.5731" height="3.25731" rx="1.62866" transform="matrix(-0.706822 0.707391 -0.706822 -0.707391 25.9414 2.38123)" fill="#FFFFFF"/></svg></div>
        <strong>Ошыбка! Что-то пошло не так.</strong>
        <p> Сообщение не отправлено!</p>
        <button class="btn black modal-close">Закрыть</button>
    </div>
</div>
<?php
wp_footer();
?>
</body>
</html>
